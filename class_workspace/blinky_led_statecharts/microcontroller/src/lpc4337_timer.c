/*
 * High-level abstraction layer for the timers of the lpc4337.
 * Was developed, basically, to use the interrupts of the timer easily.
 * 			Author: Marifante (Julian Rodriguez).
 */

// Includes  -------------------------------------------------------------------
#include "chip.h"
#include "../inc/lpc4337_timer.h"

// Macros & constants  ---------------------------------------------------------
/* @brief macro to choose a timer struct (made by LPCOpen developers) only
 only inserting the timer number and a pointer to that struct type. */
#define SELECT_TIMER_STRUCT(chosenTimer, pTimer)	{\
switch(chosenTimer)\
{\
	case TIMER0:\
	pTimer = LPC_TIMER0;\
	break;\
	case TIMER1:\
	pTimer = LPC_TIMER1;\
	break;\
	case TIMER2:\
	pTimer = LPC_TIMER2;\
	break;\
	case TIMER3:\
	pTimer = LPC_TIMER3;\
	break;\
	default:\
	/* Unknown timer. */\
	break;\
}\
\
}

// Private functions definition ------------------------------------------------
/* @brief set a certain frequency on a timer.
 * The frequency of the timer gonna be the frequency of the peripheral clock
 * divided the value of the PR register minus one.
 * The maths are: PPCLK * (PR-1) = P
 * Where: PCLK = 1/PCLK_MAX_FREQ , P = 1/frequency, PR = value of PR register */
void Timer_setFrequency( LPC_TIMER_T* timerStruct, uint32_t frequency )
{
//	timerStruct->PR = ( (uint32_t) PCLK_MAX_FREQ/frequency - 1 );
}


// Public functions definition -------------------------------------------------
/* @brief init a chosenTimer with a defined frequency. */
void Chip_initTimer( timerNumber_t chosenTimer, uint32_t timerFrequency )
{
	LPC_TIMER_T *timer;
	SELECT_TIMER_STRUCT( chosenTimer, timer );
	Chip_TIMER_Init( timer );

}

/* @brief init an interrupt of interruptPeriod microseconds for the timer
 * chosenTimer. */
void Chip_initTimerInterrupt( timerNumber_t chosenTimer, uint32_t interruptPeriod )
{
	LPC_TIMER_T *timer;
	SELECT_TIMER_STRUCT( chosenTimer, timer );

	//Chip_TIMER_DeInit( timer );

	uint32_t timerFreq = 20400000; // Get timer frequency
	/* If the microseconds are smaller or bigger than 1000000 then the order of
	the product must be different */
	uint32_t matchValue =	( interruptPeriod > 1000000 ) ? \
							( ( interruptPeriod / 1000000 ) * timerFreq ) :\
							( ( interruptPeriod * timerFreq ) / 1000000 );


	//Chip_TIMER_MatchEnableInt( timer, );
}
