/*
 * High-level abstraction layer for the uarts of the lpc4337.
 * Was developed, initially, to use a uart to send logs through an serial -
 * usb IC..
 * 			Author: Marifante (Julian Rodriguez).
 *
 * This code was based on the source codes of the sAPI library for EDU-CIAA:
 * Copyright 2014, Pablo Ridolfi (UTN-FRBA).
 * Copyright 2014, Juan Cecconi.
 * Copyright 2015-2017, Eric Pernia.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//// Includes  -------------------------------------------------------------------
//#include "chip.h"
#include "../inc/lpc4337_uart.h"
//
//// Private data declaration ----------------------------------------------------
//typedef void (*callBackFuncPtr_t)(void *);
//
//typedef struct
//{
//	uint8_t scuPort;
//	uint8_t scuPin;
//	uint8_t funcmode;
//} scuConfig_t;
//
//typedef struct
//{
//	scuConfig_t rx;
//	scuConfig_t tx;
//} uartScuConfig_t;
//
//// Constants -------------------------------------------------------------------
//// @brief pointers to the structs of each UART.
//static LPC_USART_T *pUARTs[ ] = { LPC_USART0, LPC_UART1, LPC_USART2, LPC_USART3 };
//
//// @brief array with the IRQn of each UART.
//static const IRQn_Type irqnUARTs[ ] = { USART0_IRQn, UART1_IRQn,
//										USART2_IRQn, USART3_IRQn };
//
//// @brief structs with the SCU configuration of each UART.
//const uartScuConfig_t scuUART0 =
//{
//		.tx = {  .scuPort = 7, .scuPin = 1, .funcmode = FUNC6 },
//		.rx = {  .scuPort = 7, .scuPin = 2, .funcmode = FUNC6 }
//
//};
//
//const uartScuConfig_t scuUART1 =
//{
//		.tx = {  .scuPort = 7, .scuPin = 1, .funcmode = FUNC6 },
//		.rx = {  .scuPort = 7, .scuPin = 2, .funcmode = FUNC6 }
//
//};
//
//const uartScuConfig_t scuUART2 =
//{
//		.tx = {  .scuPort = 7, .scuPin = 1, .funcmode = FUNC6 },
//		.rx = {  .scuPort = 7, .scuPin = 2, .funcmode = FUNC6 }
//
//};
//
//const uartScuConfig_t scuUART3 =
//{
//		.tx = {  .scuPort = 7, .scuPin = 1, .funcmode = FUNC6 },
//		.rx = {  .scuPort = 7, .scuPin = 2, .funcmode = FUNC6 }
//
//};
//
//static const uartScuConfig_t scuUARTs[ ] = { scuUART0, scuUART1, scuUART2, scuUART3 };
//
//
//// Private data definition -----------------------------------------------------
//static volatile callBackFuncPtr_t rxIsrCallbackUART2 = NULL;
//static volatile callBackFuncPtr_t txIsrCallbackUART2 = NULL;
//
//
//
//// Private functions definition ------------------------------------------------
///* @brief Function to handle the ISR of the UARTS. */
//static void uartProcessIRQ( uartNumber_t uart )
//{
//   uint8_t status = Chip_UART_ReadLineStatus( pUARTs[uart] );
//
//   if( status & UART_LSR_RDR )  //check if it is data in the RX buffer FIFO
//   {
//	   if( rxIsrCallbacks[ uart-1 ] != NULL ) //If there is a callback assigned then execute it
//		   (*rxIsrCallbacks[ uart-1 ])(0);
//   }
//   else
//   if( status & UART_LSR_THRE ) //check if it UART TX its ready
//   {
//	   if( txIsrCallbacks[ uart-1 ] != NULL ) //If there is a callback assigned then execute it
//		   (*txIsrCallbacks[ uart-1 ])(0);
//   }
//}
//
///* @brief enable or disable an RX uart interrupt. */
//void Chip_setUartRxInterrupt( uartNumber_t uart, bool enable )
//{
//	if( enable )
//	{
//	  // Enable UART Receiver Buffer Register Interrupt
//	  Chip_UART_IntEnable( pUARTs[uart], UART_IER_RBRINT );
//	  // Enable Interrupt for UART channel
//	  NVIC_EnableIRQ( irqnUARTs[uart] );
//	}
//	else
//	{
//	  // Disable UART Receiver Buffer Register Interrupt
//	  Chip_UART_IntDisable( pUARTs[uart], UART_IER_RBRINT );
//	  // Disable Interrupt for UART channel
//	  NVIC_DisableIRQ( irqnUARTs[uart] );
//   }
//}
//
///* @brief enable or disable an TX uart interrupt. */
//void Chip_uartTxInterruptSet( uartNumber_t uart, bool enable )
//{
//   if( enable )
//   {
//      // Enable THRE irq (TX)
//      Chip_UART_IntEnable( pUARTs[uart], UART_IER_THREINT );
//      NVIC_EnableIRQ( irqnUARTs[uart] );
//   } else{
//      // Disable THRE irq (TX)
//      Chip_UART_IntDisable( pUARTs[uart], UART_IER_THREINT );
//      NVIC_DisableIRQ( irqnUARTs[uart] );
//   }
//}
//
//// Public functions definition -------------------------------------------------
///* @brief config a determined UART with a certain baudRate. */
//void uartConfig( uartNumber_t uart, uint32_t baudRate )
//{
//   // Initialize UART
//   Chip_UART_Init( pUARTs[uart] );
//   // Set Baud rate
//   Chip_UART_SetBaud( pUARTs[uart], baudRate );
//   // Restart FIFOS using FCR (FIFO Control Register).
//   // Set Enable, Reset content, set trigger level
//   Chip_UART_SetupFIFOS( pUARTs[uart],
//                         UART_FCR_FIFO_EN |
//                         UART_FCR_TX_RS   |
//                         UART_FCR_RX_RS   |
//                         UART_FCR_TRG_LEV0 );
//   // Dummy read
//   Chip_UART_ReadByte( pUARTs[uart] );
//   // Enable UART Transmission
//   Chip_UART_TXEnable( pUARTs[uart] );
//   // Configure SCU UARTn_TXD pin
//   Chip_SCU_PinMux(		scuUARTs[uart].tx.scuPort,
//		   				scuUARTs[uart].tx.scuPin,
//						MD_PDN,
//						scuUARTs[uart].tx.funcmode );
//   // Configure SCU UARTn_RXD pin
//   Chip_SCU_PinMux(		scuUARTs[uart].rx.scuPort,
//                    	scuUARTs[uart].rx.scuPin,
//						MD_PLN | MD_EZI | MD_ZI,
//						scuUARTs[uart].rx.funcmode );
//
//}
//
//
//// ISR external functions definition -------------------------------------------
//__attribute__ ((section(".after_vectors")))
//
////// @brief Handler for ISR UART0 (IRQ 24)
////void UART0_IRQHandler(void){
////   uartProcessIRQ( UART0 );
////}
////
////// @brief Handler for ISR UART1 (IRQ 27)
////void UART3_IRQHandler(void){
////   uartProcessIRQ( UART1 );
////}
//
//// @brief Handler for ISR UART2 (IRQ 26)
//void UART2_IRQHandler(void)
//{
//   uartProcessIRQ( UART2 );
//}
//
////// @brief Handler for ISR UART3 (IRQ 27)
////void UART3_IRQHandler(void){
////   uartProcessIRQ( UART3 );
////}
