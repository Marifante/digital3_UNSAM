/*
 * High-level abstraction layer for the timers of the lpc4337.
 * Was developed, basically, to use the interrupts of the timer easily.
 *      Author: Marifante (Julian Rodriguez).
 */

#ifndef MICROCONTROLLER_INC_LPC4337_TIMER_H_
#define MICROCONTROLLER_INC_LPC4337_TIMER_H_


// Public variable types declaration -------------------------------------------
typedef enum
{
	TIMER0 = 0,
	TIMER1 = 1,
	TIMER2 = 2,
	TIMER3 = 3
} timerNumber_t;

// Public functions definition -------------------------------------------------
void Chip_initTimerInterrupt( timerNumber_t chosenTimer, uint32_t interruptPeriod );

#endif /* MICROCONTROLLER_INC_LPC4337_TIMER_H_ */
