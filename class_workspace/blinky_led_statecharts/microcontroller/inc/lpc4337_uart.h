/*
 * lpc4337_uart.h
 *
 *  Created on: 8 abr. 2020
 *      Author: marifante
 */



// Public data definition ------------------------------------------------------
/* @brief Enumeration of the four uarts of the LPC4337. */
typedef enum
{
	UART0 = 0,
	UART1 = 1,
	UART2 = 2,
	UART3 = 3
} uartNumber_t;

// Public function declaration -------------------------------------------------
