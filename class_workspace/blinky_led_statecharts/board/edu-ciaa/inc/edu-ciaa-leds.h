/*
 * EDU-CIAA board firmware developed by Julian Rodriguez.
 * Leds module.
 * Used to have the high-level-functions to operate with the leds included in
 * the board.
 * 			Author: Marifante (Julian Rodriguez)
 */

// Includes --------------------------------------------------------------------

// Public variable types declaration -------------------------------------------
typedef enum
{
	LED0_R	= 0,
	LED0_G	= 1,
	LED0_B	= 2,
	LED1	= 3,
	LED2	= 4,
	LED3	= 5
} boardLed_t;

typedef enum
{
	OFF = 0,
	ON	= 1
} ledState_t;

// Public functions declaration ------------------------------------------------
/* @brief init only one led of the board.*/
void Board_initLed( boardLed_t led );

/* @brief init all the leds of the board.*/
void Board_initAllLeds( void );

/* @brief toggle one led of the board.*/
void Board_toggleLed( boardLed_t led );

/* @brief set ON or OFF one led of the board. */
void Board_setLed( boardLed_t led, ledState_t ledState );
