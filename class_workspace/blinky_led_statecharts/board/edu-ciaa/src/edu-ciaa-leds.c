/*
 * EDU-CIAA board firmware developed by Julian Rodriguez.
 * Leds module.
 * Used to have the high-level-functions to operate with the leds included in
 * the board.
 * 			Author: Marifante (Julian Rodriguez)
 */

/*
 * Pinout of LPC4337 and relationship with the pinout of the EDU-CIAA:
 *							GPIO port | GPIO pin | SCU group | SCU pin | SCU func
 * LED0_R: GPIO5[0] P2_0 		5	  |		0	 |	  2 	 |	 0    |		4
 * LED0_G: GPIO5[1] P2_1 		5	  |		1	 |	  2 	 |	 1    |		4
 * LED0_B: GPIO5[2]	P2_2 		5	  |		2	 |	  2 	 |	 2    |		4
 * LED1: GPIO0[14] P2_10 		0	  |		14	 |	  2 	 |	 10   |		0
 * LED2: GPIO1[11] P2_11 		1	  |		11	 |	  2 	 |	 11   |		0
 * LED3: GPIO1[12] P2_12 		1	  |		12	 |	  2 	 |	 12   |		0
 * */


// Includes --------------------------------------------------------------------
#include "edu-ciaa-leds.h"
#include <stdint.h>
#include "chip.h"

// Macros & definition ---------------------------------------------------------
// @brief  							{ GPIO port, GPIO pin, SCU group, SCU pin, SCU func }
#define LED0_R_PORT_PINS_FUNC(p)	{p.gpioPort=5;	p.gpioPin=0;	p.scuGroup=2;	p.scuPin=0;		p.funcmode=4;}
#define LED0_G_PORT_PINS_FUNC(p)	{p.gpioPort=5;	p.gpioPin=1;	p.scuGroup=2;	p.scuPin=1;		p.funcmode=4;}
#define LED0_B_PORT_PINS_FUNC(p)	{p.gpioPort=5;	p.gpioPin=2;	p.scuGroup=2;	p.scuPin=2;		p.funcmode=4;}
#define LED1_PORT_PINS_FUNC(p)		{p.gpioPort=0;	p.gpioPin=14;	p.scuGroup=2;	p.scuPin=10; 	p.funcmode=0;}
#define LED2_PORT_PINS_FUNC(p)		{p.gpioPort=1;	p.gpioPin=11;	p.scuGroup=2;	p.scuPin=11; 	p.funcmode=0;}
#define LED3_PORT_PINS_FUNC(p)		{p.gpioPort=1;	p.gpioPin=12;	p.scuGroup=2;	p.scuPin=12; 	p.funcmode=0;}

// @brief macro to select the rigth ports and pins for a corresponding led
#define SELECT_LED_PORTS_PIN_FUNC( pinout, led ) {\
	switch( led )\
	{\
	case LED0_R:\
		LED0_R_PORT_PINS_FUNC(pinout);\
		break;\
	case LED0_G:\
		LED0_G_PORT_PINS_FUNC(pinout);\
		break;\
	case LED0_B:\
		LED0_B_PORT_PINS_FUNC(pinout);\
		break;\
	case LED1:\
		LED1_PORT_PINS_FUNC(pinout);\
		break;\
	case LED2:\
		LED2_PORT_PINS_FUNC(pinout);\
		break;\
	case LED3:\
		LED3_PORT_PINS_FUNC(pinout);\
		break;\
	default:\
		/* Unknown led. */\
		return;\
	}\
}\

// Private variables declaration -----------------------------------------------
typedef struct {
	uint8_t gpioPort;
	uint8_t gpioPin;
	uint8_t scuGroup;
	uint8_t scuPin;
	uint8_t funcmode;
} ledPinout_t;

// Public functions definition -------------------------------------------------
void Board_initLed( boardLed_t led )
{
	ledPinout_t pinout;
	SELECT_LED_PORTS_PIN_FUNC( pinout, led );
	Chip_SCU_PinMuxSet( pinout.scuGroup, pinout.scuPin, pinout.funcmode );
	Chip_GPIO_SetPinDIROutput( LPC_GPIO_PORT, pinout.gpioPort, pinout.gpioPin );
}

void Board_initAllLeds( void )
{
	Board_initLed( LED0_R );
	Board_initLed( LED0_G );
	Board_initLed( LED0_B );
	Board_initLed( LED1 );
	Board_initLed( LED2 );
	Board_initLed( LED3 );
}

void Board_toggleLed( boardLed_t led )
{
	ledPinout_t pinout;
	SELECT_LED_PORTS_PIN_FUNC( pinout, led );
	Chip_GPIO_SetPinToggle( LPC_GPIO_PORT, pinout.gpioPort, pinout.gpioPin );

}

void Board_setLed( boardLed_t led, ledState_t ledState )
{
	ledPinout_t pinout;
	SELECT_LED_PORTS_PIN_FUNC( pinout, led );

	if( ledState == OFF )
		Chip_GPIO_SetPortOutLow( LPC_GPIO_PORT, pinout.gpioPort, pinout.gpioPin );
	else
		Chip_GPIO_SetPinOutHigh( LPC_GPIO_PORT, pinout.gpioPort, pinout.gpioPin);
}
