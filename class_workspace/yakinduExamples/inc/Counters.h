/*
 * Counters.h
 *
 *  Created on: 13 abr. 2020
 *      Author: marifante
 */

#ifndef COUNTERS_H_
#define COUNTERS_H_
#define MAX_COUNT		30
#define MIN_COUNT		0

typedef enum{
	DOWNCOUNT = 0,
	UPCOUNT = 1
}CounterState_t;

typedef struct{
	uint32_t Count;
	CounterState_t state;
}Counter_t;


#endif /* COUNTERS_H_ */
