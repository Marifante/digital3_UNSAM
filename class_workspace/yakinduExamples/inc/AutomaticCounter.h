/*
 * AutomaticCounter.h
 *
 *  Created on: 13 abr. 2020
 *      Author: marifante
 */

#ifndef AUTOMATICCOUNTER_H_
#define AUTOMATICCOUNTER_H_

#define AUTOMATIC_COUNTER_STEP_SEGS			1 // Defines every how many seconds the automatic counters counts

#endif /* AUTOMATICCOUNTER_H_ */
